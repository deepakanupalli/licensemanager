/*Copyright (c) 2015-2016 wavemaker-com All Rights Reserved.This software is the confidential and proprietary information of wavemaker-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with wavemaker-com*/

package com.licensemanager.licensegen;


import com.wavemaker.runtime.javaservice.JavaServiceSuperClass;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.studio.common.json.*;
import com.wavemaker.runtime.*;
import com.wavemaker.runtime.file.model.*;

import com.wavemaker.lf.exceptions.*;
import com.wavemaker.lf.generator.*;
import com.wavemaker.lf.model.*;
import com.wavemaker.lf.model.WMLicenseConstants;
import com.wavemaker.lf.generator.WMLicenseTypes;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.*;

import org.apache.commons.io.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.util.*;
import java.io.*;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.*;
import java.util.Calendar;

import com.wavemaker.studio.json.JSONArray;
import org.springframework.core.io.FileSystemResource;

/**
 * This is a client-facing service class.  All
 * public methods will be exposed to the client.  Their return
 * values aand parameters will be passed to the client or taken
 * from the client, respectively.  This will be a singleton
 * instance, shared between all requests. 
 * 
 * To log, call the superclass method log(LOG_LEVEL, String) or log(LOG_LEVEL, String, Exception).
 * LOG_LEVEL is one of FATAL, ERROR, WARN, INFO and DEBUG to modify your log level.
 * For info on these levels, look for tomcat/log4j documentation
 */
@ExposeToClient
public class LicenseGen  {
    /* Pass in one of FATAL, ERROR, WARN,  INFO and DEBUG to modify your log level;
     *  recommend changing this to FATAL or ERROR before deploying.  For info on these levels, look for tomcat/log4j documentation
     */
    private static final Logger logger = LoggerFactory.getLogger(LicenseGen.class);

    public static int generateLicenseID() {
       int min = 100000;
       int max = 238328;
       Random rand = new Random();
       int randomNum = rand.nextInt((max - min) + 1) + min;
       return randomNum;
    }
    
    private String convertDate(String startDate){
        String start = "";
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        Date dateval = null;
        String monthVal = "";
        String dayVal = "";
        try 
        {
        	dateval = sdfDate.parse(startDate);
        } 
        catch (ParseException e) 
        {
            //e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateval);
        int month = cal.get(Calendar.MONTH)+1;
        if (month<=9)
        {
        	monthVal = "0"+month+"";
        }
        else
        {
        	monthVal = month+"";
        }
        
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

        if (dayOfMonth<=9)
        {
            dayVal = "0"+dayOfMonth+"";
        }
        else
        {
        	dayVal = dayOfMonth+"";
        }
        
        String startYearFormat = "YY";
        SimpleDateFormat sdfyear = new SimpleDateFormat(startYearFormat);
        String year = sdfyear.format(dateval);
        start = dayVal+""+monthVal+""+year;
        logger.info("----------------------------------");
        logger.info("Start date = "+startDate);
        logger.info(start);
        return start;
    }
    
    public DownloadResponse newLicenseFile(String urlfile, String newfilename, HttpServletResponse response) throws Exception {
        InputStream inStream = null;
        OutputStream outStream = null;

        try {
            inStream = new FileInputStream(urlfile);
            response.setHeader("Content-Type", "application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename="+newfilename);
            outStream = response.getOutputStream();
            byte[] buffer = new byte[4096]; // 4096 is kind of a magic number
            int readBytesCount = 0;
            while((readBytesCount = inStream.read(buffer)) >= 0) {
                outStream.write(buffer, 0, readBytesCount);
            }
            IOUtils.copy(inStream, outStream);
            response.flushBuffer();
            outStream.flush();
            
            DownloadResponse downloadResponse = new DownloadResponse(inStream, null, newfilename);
            downloadResponse.setInline(false);
            return downloadResponse;
        } catch (Exception e) {
            logger.error("Failed to download the license key", e);
            throw e;
        }
        
        finally {
            IOUtils.closeQuietly(inStream);
            IOUtils.closeQuietly(outStream);
        }
        // Resource downloadFile = new FileSystemResource(urlfile);
        // return new downloadFile.getFile();
    }
	public String generateLicense2(String json, String invoiceid, HttpServletResponse response) throws Exception
	{
	    List list = JSONUtils.toObject(json, List.class);
	    ArrayList<Properties> propertiesList = new ArrayList<Properties>();

	    for (int i=0;i<list.size();i++) {
	        Map map = (Map) list.get(i);
	       
            Properties props = new Properties();
            
            String skucode = (String)map.get("skucode");
            props.setProperty(WMLicenseConstants.SKU_CODE_PARAMETER, skucode);
            String licenseid = generateLicenseID()+"";
            props.setProperty(WMLicenseConstants.LICENSE_ID_PARAMETER, licenseid);
            props.setProperty(WMLicenseConstants.LICENSE_TYPE_PARAMETER, WMLicenseTypes.LIMITED.getLicenseTypeId()+"");
            String quantity = (int)map.get("quantity")+"";
            props.setProperty(WMLicenseConstants.QUANTITY_PARAMETER, quantity);
            String startDate = convertDate((String)map.get("startdate"));
            logger.info(startDate);
            props.setProperty(WMLicenseConstants.START_DATE_PARAMETER, startDate);
            String duration =  (int)map.get("duration")+"";
            props.setProperty(WMLicenseConstants.DURATION_PARAMETER, duration);
            props.setProperty(WMLicenseConstants.UNIT_MULTIPLIER_PARAMETER, "1");
            String totaldev = (int)map.get("totaldev")+"";
            props.setProperty(WMLicenseConstants.UNITS_DEVELOPERS_PARAMETER, totaldev);
            String totalnodes = (int)map.get("totalnodes")+"";
            props.setProperty(WMLicenseConstants.UNITS_NODES_PARAMETER, totalnodes);
            String totalhits = (int)map.get("totalhits")+"";
            String custName = (String)map.get("customername")+"";
            props.setProperty(WMLicenseConstants.UNITS_HITS_PARAMETER, totalhits);
            props.setProperty(WMLicenseConstants.PRODUCT_NAME, (String)map.get("productname"));
            props.setProperty(WMLicenseConstants.LICENSEE, (String)map.get("customername"));
            props.setProperty(WMLicenseConstants.LICENSE_INVOICE_ID, invoiceid);
            props.setProperty(WMLicenseConstants.DESCRIPTION, (String)map.get("description"));
            logger.info(props+"");
            propertiesList.add(props);
	    }
	    String newfileName = "License_"+invoiceid+".zip";
        LicenseGeneratorImpl licGen = new LicenseGeneratorImpl();
        try {
            //String fileName = licGen.createLicenses(propertiesList);
            return licGen.createLicenses(propertiesList);
        } catch (WrongKeyParamInputException e) {
            //logger.info(props+"");
            logger.error("Failed to download the license key", e);
            throw e;
        }
	}
}


