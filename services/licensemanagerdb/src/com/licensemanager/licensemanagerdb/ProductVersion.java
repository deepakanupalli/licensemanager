/*Copyright (c) 2015-2016 imaginea-com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea-com*/

package com.licensemanager.licensemanagerdb;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;



/**
 * ProductVersion generated by hbm2java
 */
@Entity
@Table(name="`ProductVersion`"
    ,schema="licensemanagerdb"
)

public class ProductVersion  implements java.io.Serializable {

    private Integer versionId;
    private String createdBy;
    private Date createdDate;
    private String imageUrl;
    private String modifiedBy;
    private Date modifiedDate;
    private Date releaseDate;
    private String releaseVersion;
    private Product product;

    public ProductVersion() {
    }


    @Id @GeneratedValue(strategy=IDENTITY)
    

    @Column(name="`versionID`", nullable=false)
    public Integer getVersionId() {
        return this.versionId;
    }
    
    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }

    

    @Column(name="`createdBy`", length=80)
    public String getCreatedBy() {
        return this.createdBy;
    }
    
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.DATE)

    @Column(name="`createdDate`", length=10)
    public Date getCreatedDate() {
        return this.createdDate;
    }
    
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    

    @Column(name="`imageURL`", length=1024)
    public String getImageUrl() {
        return this.imageUrl;
    }
    
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    

    @Column(name="`modifiedBy`", length=80)
    public String getModifiedBy() {
        return this.modifiedBy;
    }
    
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Temporal(TemporalType.DATE)

    @Column(name="`modifiedDate`", length=10)
    public Date getModifiedDate() {
        return this.modifiedDate;
    }
    
    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Temporal(TemporalType.DATE)

    @Column(name="`releaseDate`", length=10)
    public Date getReleaseDate() {
        return this.releaseDate;
    }
    
    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    

    @Column(name="`releaseVersion`", length=25)
    public String getReleaseVersion() {
        return this.releaseVersion;
    }
    
    public void setReleaseVersion(String releaseVersion) {
        this.releaseVersion = releaseVersion;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="`productID`", nullable=false)
    public Product getProduct() {
        return this.product;
    }
    
    public void setProduct(Product product) {
        this.product = product;
    }





    public boolean equals(Object o) {
         if (this == o) return true;
		 if ( (o == null )) return false;
		 if ( !(o instanceof ProductVersion) )
		    return false;

		 ProductVersion that = (ProductVersion) o;

		 return ( (this.getVersionId()==that.getVersionId()) || ( this.getVersionId()!=null && that.getVersionId()!=null && this.getVersionId().equals(that.getVersionId()) ) );
    }

    public int hashCode() {
         int result = 17;

         result = 37 * result + ( getVersionId() == null ? 0 : this.getVersionId().hashCode() );

         return result;
    }


}

