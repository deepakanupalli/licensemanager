/*Copyright (c) 2015-2016 imaginea-com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea-com*/

package com.licensemanager.licensemanagerdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/





import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.licensemanager.licensemanagerdb.*;


/**
 * ServiceImpl object for domain model class Licenses.
 * @see com.licensemanager.licensemanagerdb.Licenses
 */
@Service("licensemanagerdb.LicensesService")
public class LicensesServiceImpl implements LicensesService {


    private static final Logger LOGGER = LoggerFactory.getLogger(LicensesServiceImpl.class);

    @Autowired
    @Qualifier("licensemanagerdb.LicensesDao")
    private WMGenericDao<Licenses, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Licenses, Integer> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }
     @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
     public Page<Licenses> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable){
          LOGGER.debug("Fetching all associated");
          return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
     }

    @Transactional(value = "licensemanagerdbTransactionManager")
    @Override
    public Licenses create(Licenses licenses) {
        LOGGER.debug("Creating a new licenses with information: {}" , licenses);
        return this.wmGenericDao.create(licenses);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "licensemanagerdbTransactionManager")
    @Override
    public Licenses delete(Integer licensesId) throws EntityNotFoundException {
        LOGGER.debug("Deleting licenses with id: {}" , licensesId);
        Licenses deleted = this.wmGenericDao.findById(licensesId);
        if (deleted == null) {
            LOGGER.debug("No licenses found with id: {}" , licensesId);
            throw new EntityNotFoundException(String.valueOf(licensesId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public Page<Licenses> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all licensess");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public Page<Licenses> findAll(Pageable pageable) {
        LOGGER.debug("Finding all licensess");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public Licenses findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding licenses by id: {}" , id);
        Licenses licenses=this.wmGenericDao.findById(id);
        if(licenses==null){
            LOGGER.debug("No licenses found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return licenses;
    }
    @Transactional(rollbackFor = EntityNotFoundException.class, value = "licensemanagerdbTransactionManager")
    @Override
    public Licenses update(Licenses updated) throws EntityNotFoundException {
        LOGGER.debug("Updating licenses with information: {}" , updated);
        this.wmGenericDao.update(updated);

        Integer id = (Integer)updated.getLicenseId();

        return this.wmGenericDao.findById(id);
    }

    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


