/*Copyright (c) 2015-2016 imaginea-com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea-com*/

package com.licensemanager.licensemanagerdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/





import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.licensemanager.licensemanagerdb.*;


/**
 * ServiceImpl object for domain model class ProductSkucode.
 * @see com.licensemanager.licensemanagerdb.ProductSkucode
 */
@Service("licensemanagerdb.ProductSkucodeService")
public class ProductSkucodeServiceImpl implements ProductSkucodeService {


    private static final Logger LOGGER = LoggerFactory.getLogger(ProductSkucodeServiceImpl.class);

    @Autowired
    @Qualifier("licensemanagerdb.ProductSkucodeDao")
    private WMGenericDao<ProductSkucode, String> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<ProductSkucode, String> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }
     @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
     public Page<ProductSkucode> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable){
          LOGGER.debug("Fetching all associated");
          return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
     }

    @Transactional(value = "licensemanagerdbTransactionManager")
    @Override
    public ProductSkucode create(ProductSkucode productskucode) {
        LOGGER.debug("Creating a new productskucode with information: {}" , productskucode);
        return this.wmGenericDao.create(productskucode);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "licensemanagerdbTransactionManager")
    @Override
    public ProductSkucode delete(String productskucodeId) throws EntityNotFoundException {
        LOGGER.debug("Deleting productskucode with id: {}" , productskucodeId);
        ProductSkucode deleted = this.wmGenericDao.findById(productskucodeId);
        if (deleted == null) {
            LOGGER.debug("No productskucode found with id: {}" , productskucodeId);
            throw new EntityNotFoundException(String.valueOf(productskucodeId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public Page<ProductSkucode> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all productskucodes");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public Page<ProductSkucode> findAll(Pageable pageable) {
        LOGGER.debug("Finding all productskucodes");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public ProductSkucode findById(String id) throws EntityNotFoundException {
        LOGGER.debug("Finding productskucode by id: {}" , id);
        ProductSkucode productskucode=this.wmGenericDao.findById(id);
        if(productskucode==null){
            LOGGER.debug("No productskucode found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return productskucode;
    }
    @Transactional(rollbackFor = EntityNotFoundException.class, value = "licensemanagerdbTransactionManager")
    @Override
    public ProductSkucode update(ProductSkucode updated) throws EntityNotFoundException {
        LOGGER.debug("Updating productskucode with information: {}" , updated);
        this.wmGenericDao.update(updated);

        String id = (String)updated.getSkucode();

        return this.wmGenericDao.findById(id);
    }

    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


