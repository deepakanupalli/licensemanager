/*Copyright (c) 2015-2016 imaginea-com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea-com*/

package com.licensemanager.licensemanagerdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/





import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.licensemanager.licensemanagerdb.*;


/**
 * ServiceImpl object for domain model class Users.
 * @see com.licensemanager.licensemanagerdb.Users
 */
@Service("licensemanagerdb.UsersService")
public class UsersServiceImpl implements UsersService {


    private static final Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class);

    @Autowired
    @Qualifier("licensemanagerdb.UsersDao")
    private WMGenericDao<Users, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Users, Integer> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }
     @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
     public Page<Users> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable){
          LOGGER.debug("Fetching all associated");
          return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
     }

    @Transactional(value = "licensemanagerdbTransactionManager")
    @Override
    public Users create(Users users) {
        LOGGER.debug("Creating a new users with information: {}" , users);
        return this.wmGenericDao.create(users);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "licensemanagerdbTransactionManager")
    @Override
    public Users delete(Integer usersId) throws EntityNotFoundException {
        LOGGER.debug("Deleting users with id: {}" , usersId);
        Users deleted = this.wmGenericDao.findById(usersId);
        if (deleted == null) {
            LOGGER.debug("No users found with id: {}" , usersId);
            throw new EntityNotFoundException(String.valueOf(usersId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public Page<Users> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all userss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public Page<Users> findAll(Pageable pageable) {
        LOGGER.debug("Finding all userss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public Users findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding users by id: {}" , id);
        Users users=this.wmGenericDao.findById(id);
        if(users==null){
            LOGGER.debug("No users found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return users;
    }
    @Transactional(rollbackFor = EntityNotFoundException.class, value = "licensemanagerdbTransactionManager")
    @Override
    public Users update(Users updated) throws EntityNotFoundException {
        LOGGER.debug("Updating users with information: {}" , updated);
        this.wmGenericDao.update(updated);

        Integer id = (Integer)updated.getUserId();

        return this.wmGenericDao.findById(id);
    }

    @Transactional(readOnly = true, value = "licensemanagerdbTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


