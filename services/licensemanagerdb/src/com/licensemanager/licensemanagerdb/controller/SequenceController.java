/*Copyright (c) 2015-2016 imaginea-com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea-com*/

package com.licensemanager.licensemanagerdb.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.licensemanager.licensemanagerdb.service.SequenceService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wavemaker.runtime.file.model.DownloadResponse;
import com.wordnik.swagger.annotations.*;
import com.licensemanager.licensemanagerdb.*;
import com.licensemanager.licensemanagerdb.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class Sequence.
 * @see com.licensemanager.licensemanagerdb.Sequence
 */
@RestController(value = "Licensemanagerdb.SequenceController")
@RequestMapping("/licensemanagerdb/Sequence")
@Api(description = "Exposes APIs to work with Sequence resource.", value = "SequenceController")
public class SequenceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SequenceController.class);

    @Autowired
    @Qualifier("licensemanagerdb.SequenceService")
    private SequenceService sequenceService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of Sequence instances matching the search criteria.")
    public Page<Sequence> findSequences(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Sequences list");
        return sequenceService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of Sequence instances.")
    public Page<Sequence> getSequences(Pageable pageable) {
        LOGGER.debug("Rendering Sequences list");
        return sequenceService.findAll(pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setSequenceService(SequenceService service) {
        this.sequenceService = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Creates a new Sequence instance.")
    public Sequence createSequence(@RequestBody Sequence instance) {
        LOGGER.debug("Create Sequence with information: {}", instance);
        instance = sequenceService.create(instance);
        LOGGER.debug("Created Sequence with information: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of Sequence instances.")
    public Long countAllSequences() {
        LOGGER.debug("counting Sequences");
        Long count = sequenceService.countAll();
        return count;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the Sequence instance associated with the given id.")
    public Sequence getSequence(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting Sequence with id: {}", id);
        Sequence instance = sequenceService.findById(id);
        LOGGER.debug("Sequence details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Updates the Sequence instance associated with the given id.")
    public Sequence editSequence(@PathVariable("id") Integer id, @RequestBody Sequence instance) throws EntityNotFoundException {
        LOGGER.debug("Editing Sequence with id: {}", instance.getId());
        instance.setId(id);
        instance = sequenceService.update(instance);
        LOGGER.debug("Sequence details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Deletes the Sequence instance associated with the given id.")
    public boolean deleteSequence(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Sequence with id: {}", id);
        Sequence deleted = sequenceService.delete(id);
        return deleted != null;
    }
}
