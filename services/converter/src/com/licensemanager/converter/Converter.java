/*Copyright (c) 2015-2016 imaginea-com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea-com*/

package com.licensemanager.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.licensemanager.converter.model.*;
import com.wavemaker.runtime.service.annotations.ExposeToClient;

/**
 * This is a singleton class with all of its public methods exposed to the client via controller.
 * Their return values and parameters will be passed to the client or taken
 * from the client respectively.
 */
@ExposeToClient
public class Converter {

    private static final Logger logger=LoggerFactory.getLogger(Converter.class);

    public String splitter(int x){
     return ""+(x/10000)+"-"+""+(x%10000);
    }
    
}
