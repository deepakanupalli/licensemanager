Application.$controller("LicensesNewPageController", ["$scope", "$window", "$http", "$timeout",
    function($scope, $window, $http, $timeout) {
        "use strict";

        /* perform any action with the variables inside this block(on-page-load) */
        $scope.onPageVariablesReady = function() {
            /*
             * variables can be accessed through '$scope.Variables' property here
             * e.g. $scope.Variables.staticVariable1.getData()
             */
        };

        /* perform any action with widgets inside this block */
        $scope.onPageReady = function() {
            /*
             * widgets can be accessed through '$scope.Widgets' property here
             * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
             */
        };





        $scope.button4Click = function($event, $isolateScope) {
            var getContent = $scope.Widgets.grid2.selecteditem;
            var x = getContent;
            if ($scope.Widgets.grid2.selecteditem.length <= 1 || $scope.Widgets.grid2.selecteditem.length === undefined) {
                x = [getContent];
            }
            var invoiceInfo = JSON.stringify(x);

            $scope.Variables.LicenseGenGenerateLicense2.dataBinding.json = invoiceInfo;
            //$scope.Variables.LicenseGenGenerateLicense2.dataBinding.invoiceid = $scope.Widgets.invList.datavalue;
            $scope.Variables.LicenseGenGenerateLicense2.update({}, function() {
                var fileName = $scope.Variables.LicenseGenGenerateLicense2.dataSet.value;
                fileName = fileName.replace("//", "/");
                var getfileName = fileName.split("/");
                var zipFileName = getfileName[getfileName.length - 1];
                var pathValue = ($window.location.pathname).replace("/index.html", "");
                console.log($window.location.origin + fileName.replace("/tmp/lf/WaveMakerLicenses", "/license"));
                var downloadFile = pathValue + "/services/licenseGen/newLicenseFile?urlfile=" + fileName + "&newfilename=" + zipFileName;
                console.log(downloadFile);
                $scope.Widgets.download.show = true;
                $scope.Widgets.download.hyperlink = downloadFile;
            });

        };
    }

]);


Application.$controller("grid2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;



    }
]);

Application.$controller("livefilter1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);