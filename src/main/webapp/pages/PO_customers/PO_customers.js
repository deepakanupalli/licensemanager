Application.$controller("PO_customersPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
        //$scope.Variables.current_date.dataSet.dataValue = new Date().getTime();
    };




}]);

Application.$controller("PolineItemGrid2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialog2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.checkbox1Change = function($event, $isolateScope) {
            if ($scope.Widgets.checkbox1.datavalue === true) {
                var z = $scope.Variables.LicensemanagerdbSequenceData.dataSet.data[0].evalCount;
                $scope.Widgets.PO_ponumber.readonly = true;
                $scope.Widgets.PO_ponumber.datavalue = "EVAL" + z;
            } else {
                $scope.Widgets.PO_ponumber.readonly = false;
                $scope.Widgets.PO_ponumber.datavalue = "";
            }
        };


        $scope.form3Submit = function($event, $isolateScope, $formData) {
            if ($scope.Widgets.checkbox1.datavalue === true) {
                $scope.Variables.increment_eval_license.update();
            }
        };

    }
]);

Application.$controller("dialog2_1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.text14Change = function($event, $isolateScope) {
            //debugger;
            var d = new Date($scope.Widgets.start_date.datavalue);
            d.setMonth(d.getMonth() + $scope.Widgets.text14.datavalue);
            $scope.Widgets.end_Date.datavalue = d;
        };


        $scope.start_dateChange = function($event, $isolateScope) {
            //debugger;
            var d = new Date($scope.Widgets.start_date.datavalue);
            d.setMonth(d.getMonth() + $scope.Widgets.text14.datavalue);
            $scope.Widgets.end_Date.datavalue = d;
        };

    }
]);

Application.$controller("grid3Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("PO_details_dialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("pagedialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialog3Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("PO_Details_ViewController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);