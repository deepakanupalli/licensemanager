Application.$controller("Create_InvoicePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };


    $scope.CustomerInvoicesLiveform1Beforeservicecall = function($event, $operation, $data) {

    };


    $scope.CustomerInvoicesGrid2Select = function($event, $data) {

    };

}]);


Application.$controller("CustomerInvoicesGrid1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("CustomerInvoicesLiveform1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;


        $scope.customerChange = function($event, $isolateScope) {
            $scope.Variables.findAssociatedPOs.setInput("id", $isolateScope.datavalue.customerId);
            $scope.Variables.findAssociatedPOs.update();

        };

    }
]);