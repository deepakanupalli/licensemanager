Application.$controller("New_InvoicesPageController", ["$scope", function($scope) {
    "use strict";
    var polData;
    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };

    $scope.grid1Select = function($event, $data) {
        createArray()
    };


    $scope.grid1Deselect = function($event, $data) {
        createArray()
    };

    function createArray() {
        var data = $scope.Widgets.grid1.selecteditem;
        if (Array.isArray(data) && data.length) {
            polData = data;
        } else if (!Array.isArray(data)) {
            polData = Array(data);
        } else {
            polData = undefined;
        }
    }

    // $scope.button1Click = function($event, $isolateScope) {
    //     polData.forEach(function(arrayItem) {
    //         $scope.Variables.update_polineitem_invoiceID.setInput('poLiID', arrayItem.poLineID);
    //         $scope.Variables.update_polineitem_invoiceID.update();

    //     });
    // };


    $scope.form2Submit = function($event, $isolateScope, $formData) {
        var selectdata = $scope.Widgets.grid2.selectedItems;
        selectdata.forEach(function(arrayItem) {
            $scope.Variables.update_poline_wmrefnum.setInput('poLiID', arrayItem.poLineID);
            $scope.Variables.update_poline_wmrefnum.update();


        })
    };


    $scope.form1Submit = function($event, $isolateScope, $formData) {
        polData.forEach(function(arrayItem) {
            $scope.Variables.update_polineitem_invoiceID.setInput('poLiID', arrayItem.poLineID);
            $scope.Variables.update_polineitem_invoiceID.update();

        });
    };


}]);








Application.$controller("grid1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grid2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);